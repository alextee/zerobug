DEBUG=True
SECRET_KEY='dev'
MAIL_DEBUG=True
MAIL_SERVER='mail.example.org'
MAIL_PORT=587
MAIL_USE_SSL=False
MAIL_USE_TLS=True
MAIL_USERNAME='example'
MAIL_PASSWORD='password'
TEST_RECIPIENT='me@example.com'
