Zerobug
=======

A simple, javascript-free issue tracker.

# Dependencies

- flask
- Flask-Mail
- Flask-WTF

Install all with pip

# Development

After creating the database (see below),
`FLASK_ENV=development FLASK_APP=zerobug flask run`

# Deployment (Apache)

1. git clone into /var/www/
2. copy the config-example.py into config.py and
set the variables accordingly
3. chown to the user that apache uses, or group only
(`chown -R me:mygroup /var/www/zerobug`)
4. `cd /var/www/zerobug` and init the database:
```
FLASK_APP=zerobug flask init-db
```
5. add the following config where apache can read (https)

```apacheconf
<IfModule mod_ssl.c>
<VirtualHost *:443>
ServerName issues.example.org

WSGIDaemonProcess zerobug threads=5
WSGIScriptAlias / /var/www/zerobug/zerobug.wsgi

<Directory /var/www/zerobug>
	WSGIProcessGroup zerobug
	WSGIApplicationGroup %{GLOBAL}
	Order deny,allow
	Allow from all
	Require all granted
</Directory>

Include /etc/letsencrypt/options-ssl-apache.conf
SSLCertificateFile # abbreviated
SSLCertificateKeyFile # abbreviated
SSLCertificateChainFile # abbreviated
</VirtualHost>
</IfModule>
```

# License
Copyright (C) 2019 Alexandros Theodotou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
