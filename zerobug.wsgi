#!/usr/bin/python

import sys

sys.path.insert(0,'/var/www/zerobug/')

from zerobug import create_app

application = create_app()
