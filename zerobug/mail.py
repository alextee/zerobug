from flask import current_app, g
from flask.cli import with_appcontext

from flask_mail import Mail

def get_mail():
    if 'mail' not in g:
        g.mail = Mail(current_app)

    return g.mail
