DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS issue;
DROP TABLE IF EXISTS status;
DROP TABLE IF EXISTS severity;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS os;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE severity (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT NOT NULL,
  description TEXT NOT NULL
);

INSERT INTO severity (
  title, description)
VALUES
  ('1 - Wish', ''),
  ('2 - Minor', ''),
  ('3 - Normal', ''),
  ('4 - Important', ''),
  ('5 - Blocker', ''),
  ('6 - Security', '');

CREATE TABLE category (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT NOT NULL,
  description TEXT NOT NULL
);

INSERT INTO category (
  title, description)
VALUES
  ('Build', ''),
  ('DSP', ''),
  ('Dev docs', ''),
  ('Manual', ''),
  ('None', ''),
  ('Packaging', ''),
  ('Performance', ''),
  ('Plugin', ''),
  ('User interface', ''),
  ('Website', '');

CREATE TABLE status (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT NOT NULL,
  description TEXT NOT NULL
);

INSERT INTO status (
  title, description)
VALUES
  ('None', ''),
  ('Done', ''),
  ('Won''t do', ''),
  ('Works for me', ''),
  ('Ready for test', ''),
  ('In progress', ''),
  ('Postponed', ''),
  ('Confirmed', ''),
  ('Need info', ''),
  ('Duplicate', ''),
  ('Invalid', '');

-- operating system
CREATE TABLE os (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

INSERT INTO os (
  name)
VALUES
  ('None'),
  ('GNU/Linux'),
  ('*BSD'),
  ('Other');

-- issue
CREATE TABLE issue (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  -- NULL if anonymous
  author_id INTEGER,
  assigned_to_id INTEGER,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  severity_id INTEGER NOT NULL,
  category_id INTEGER NOT NULL,
  status_id INTEGER NOT NULL,
  os_id INTEGER NOT NULL,
  -- 1 for open, 0 for closed
  open INTEGER NOT NULL DEFAULT 1,
  discussion_lock INTEGER NOT NULL DEFAULT 0,
  FOREIGN KEY (author_id) REFERENCES user (id),
  FOREIGN KEY (assigned_to_id) REFERENCES user (id),
  FOREIGN KEY (severity_id) REFERENCES severity (id),
  FOREIGN KEY (category_id) REFERENCES category (id),
  FOREIGN KEY (status_id) REFERENCES status (id)
  FOREIGN KEY (os_id) REFERENCES os (id)
);

-- issue comments
CREATE TABLE comment (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  issue_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  body TEXT NOT NULL,
  FOREIGN KEY (author_id) REFERENCES user (id),
  FOREIGN KEY (issue_id) REFERENCES issue (id)
);
