from flask import (
    Blueprint, flash, g, redirect, render_template,
    request, url_for, session, current_app as app
)
from werkzeug.exceptions import abort
from flask_mail import Message

from zerobug.auth import login_required
from zerobug.db import get_db
from zerobug.mail import get_mail

import datetime

bp = Blueprint('issues', __name__)

@bp.route('/')
def index():
    db = get_db()
    issues = db.execute(
        'SELECT p.id, p.title, p.body, created, modified,'
        '   author_id, u.username, c.title as category,'
        '   s.title as severity, status.title as status,'
        '   ua.username as assigned_to'
        ' FROM issue p'
        ' LEFT JOIN user u ON p.author_id = u.id'
        ' LEFT JOIN user ua ON p.assigned_to_id = ua.id'
        ' LEFT JOIN category c ON p.category_id = c.id'
        ' LEFT JOIN severity s ON p.severity_id = s.id'
        ' LEFT JOIN status status ON p.status_id = status.id'
        ' ORDER BY created DESC'
    ).fetchall()
    # mail = get_mail()
    # print ('{} {}'.format(mail.server, mail.port))
    # msg = Message("Hello",
                  # sender=app.config['MAIL_USERNAME'],
                  # recipients=[
                      # app.config['TEST_RECIPIENT'],
                      # ])
    # mail.send(msg)

    return render_template('issues/index.html', issues=issues)

# searches in the table for the given id and returns if
# found or not
def id_exists(table,id):
    db = get_db()
    count = db.execute(
        'SELECT COUNT(1) as count'
        ' FROM ' + table +
        ' WHERE id = ?',
        (id)).fetchone()
    return count['count'] == 1

@bp.route('/create', methods=('GET', 'POST'))
def create():
    db = get_db()
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        category = request.form['category']
        severity = request.form['severity']
        status = request.form['status']
        os = request.form['os']
        city = request.form['city']
        error = None

        num_open_issues = db.execute(
            'SELECT COUNT(1) as count'
            ' FROM issue'
            ' WHERE open = 1').fetchone()

        if city:
            error = 'Invalid data.'
        elif not title:
            error = 'Title is required.'
        elif (id_exists('category', category) is False):
            error = 'Invalid category'
        elif (id_exists('severity', severity) is False):
            error = 'Invalid severity'
        elif (id_exists('status', status) is False):
            error = 'Invalid status'
        elif (id_exists('os', os) is False):
            error = 'Invalid os'
        elif (num_open_issues['count'] > 150):
            error = 'Too many issues, please contact the administrator'
        else:
            # wait 30 seconds between issue updates
            if g.user is None or g.user['id'] != 'admin':
                last_update = db.execute(
                    'SELECT modified'
                    ' FROM issue'
                    ' ORDER BY modified DESC',
                    ).fetchone()
                if (last_update is not None and
                   last_update['modified'] is not None and
                   (datetime.datetime.utcnow() -
                       last_update['modified']).total_seconds() < 30):
                    error = 'Too many issues being created. Please wait a bit'


        if error is not None:
            flash(error)
        else:
            if g.user is None:
                user_id = None
            else:
                user_id = g.user['id']

            db.execute(
                'INSERT INTO issue ('
                '   title, body, severity_id, category_id,'
                '   status_id, os_id, author_id)'
                ' VALUES (?, ?, ?, ?, ?, ?, ?)',
                (title, body, severity, category, status,
                    os, user_id)
            )
            db.commit()
            return redirect(url_for('issues.index'))

    categories = db.execute(
        'SELECT id, title'
        ' FROM category p'
        ' ORDER BY id ASC'
    ).fetchall()
    severities = db.execute(
        'SELECT id, title'
        ' FROM severity p'
        ' ORDER BY id ASC'
    ).fetchall()
    statuses = db.execute(
        'SELECT id, title'
        ' FROM status p'
        ' ORDER BY id ASC'
    ).fetchall()
    oses = db.execute(
        'SELECT id, name'
        ' FROM os p'
        ' ORDER BY id ASC'
    ).fetchall()

    return render_template('issues/create.html',
            categories=categories, severities=severities,
            statuses=statuses, oses=oses)

def get_issue(id, check_author=True):
    issue = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM issue p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if issue is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and issue['author_id'] != g.user['id']:
        abort(403)

    return issue

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    issue = get_issue(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE issue SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('issues.index'))

    return render_template('issues/update.html', issue=issue)
